import cairo

import gi

gi.require_version('PangoCairo', '1.0')
from gi.repository import PangoCairo, Pango

width = 640
height = 480

sur = cairo.ImageSurface(cairo.FORMAT_ARGB32, width, height)
ctx = cairo.Context(sur)


if False:
    ctx.set_source_rgb(1, 1, 1)
    ctx.rectangle(0, 0, width, height)
    ctx.fill()

font_map = PangoCairo.font_map_get_default()


y = height * .2

font_name = "DejaVu Sans"

class Snippet:
    def __init__(self, msg):
        self.layout = PangoCairo.create_layout(ctx)

        desc = Pango.FontDescription()
        desc.set_family(font_name)
        desc.set_weight(Pango.Weight.BOLD)
        font_size = height * 0.042
        desc.set_size(font_size * Pango.SCALE)
        self.layout.set_font_description(desc)

        self.layout.set_markup(msg)

        self.layout.set_alignment(Pango.Alignment.LEFT)
        _, log_box = self.layout.get_extents()

        self.width = log_box.width / Pango.SCALE
        self.height = log_box.height / Pango.SCALE

    def draw(self, x, y):
        ctx.move_to(x, y)
        ctx.set_source_rgb(0, 0, 0)
        PangoCairo.show_layout(ctx, self.layout)

title = Snippet("The unfa Community")

x = width / 2 - title.width / 2
y = height * 0.12
title.draw(x, y)

y += title.height * 2.5

left = []
left.append(Snippet('Music'))
left.append(None)
left.append(Snippet('Vocals'))
left.append(Snippet(''))
left.append(None)
left.append(Snippet('Video'))

right = []
right.append(Snippet('mylofy'))
right.append(None)
right.append(Snippet('mylofy'))
right.append(Snippet('snuffles'))
right.append(None)
right.append(Snippet('pace'))

left_width = max([s.width for s in left if s is not None])
right_width = max([s.width for s in right if s is not None])

line_height = max([s.height for s in left + right if s is not None])

hspace = 50

total_width = left_width + hspace + right_width
indent = width / 2 - total_width / 2

x1 = indent
x2 = indent + left_width + hspace

for l, r in zip(left, right):
    if l:
        l.draw(x1, y)
    if r:
        r.draw(x2, y)

    if l is None:
        y += line_height * 1.5
    else:
        y += line_height * 1.1


sur.write_to_png("f.png")
