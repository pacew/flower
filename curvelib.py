from typing import Union, List, Optional, Tuple, Any, cast, Dict

import math
import random
import sys
import time

import bezier
import cairo
import gi
import mido

import colorsys

start_notes = 8
start_kick = 8
start_hh = 17

gi.require_version('Gtk', '3.0')
from gi.repository import GLib, Gtk  # noqa:E402

branch_color = 0x00c900

bpm = 112.0

def bars_to_secs(bars):
    return bars * 4 / bpm * 60

def secs_to_bars(secs):
    return secs / 60 * bpm / 4

def get_barnum(secs: float) -> float:
    return math.floor(secs / 60 * bpm) / 4


def set_color(ctx: cairo.Context, hexval: int, alpha: float = 1.0) -> None:
    r = ((hexval >> 16) & 0xff) / 255.0
    g = ((hexval >> 8) & 0xff) / 255.0
    b = (hexval & 0xff) / 255.0
    ctx.set_source_rgba(r, g, b, alpha)

def rtod(val: float) -> float:
    return val * 180 / math.pi

def lscale(val: float, 
           from_left: float, from_right: float, 
           to_left: float, to_right: float) -> float:
    if from_left == from_right:
        return to_right

    return ((val - from_left) / (from_right - from_left) * (to_right - to_left) + to_left)

def lscale_clamp(val: float, 
                 from_left: float, from_right: float, 
                 to_left: float, to_right: float) -> float:
    if from_left == from_right:
        return to_right

    x = ((val - from_left) / (from_right - from_left) * (to_right - to_left) + to_left)
    if to_left < to_right:
        if x < to_left:
            x = to_left
        if x > to_right:
            x = to_right
    else:
        if x < to_right:
            x = to_right
        if x > to_left:
            x = to_left
    return x

class Ang:
    def __init__(self, radians: float = 0) -> None:
        self.r = radians
        self.d = radians * 180 / math.pi

    def __repr__(self) -> str:
        return f'<{self.d:.2f}>'

    def __add__(self, other: 'Ang') -> 'Ang':
        assert isinstance(other, Ang)
        return rad(self.r + other.r)

    def __sub__(self, other: 'Ang') -> 'Ang':
        assert isinstance(other, Ang)
        return rad(self.r - other.r)

    def __neg__(self) -> 'Ang':
        return rad(-1 * self.r)


def deg(degrees: float) -> Ang:
    return Ang(degrees * math.pi / 180)

def rad(radians: Union[float, Ang]) -> Ang:
    if isinstance(radians, Ang):
        return radians
    return Ang(radians)


class Vector:
    def __init__(self, arg1: Union[Ang, float], arg2: float) -> None:
        if isinstance(arg1, Ang):
            self.direction = arg1
            self.magnitude = arg2
            self.x = self.magnitude * math.cos(self.direction.r)
            self.y = self.magnitude * math.sin(self.direction.r)
        else:
            self.x = arg1
            self.y = arg2
            self.direction = rad(math.atan2(self.y, self.x))
            self.magnitude = math.hypot(self.y, self.x)

    @classmethod
    def polar(cls, radians: Union[float, Ang], magnitude: float) -> 'Vector':
        return cls(rad(radians), magnitude)

    def __repr__(self) -> str:
        return f'<Vector dir={self.direction.d:.2f}' \
            f' mag={self.magnitude:.2f}' \
            f' ({self.x:.1f}, {self.y:.1f})>'

    def __add__(self, other: 'Vector') -> 'Vector':
        return Vector(self.x + other.x, self.y + other.y)

    def __sub__(self, other: 'Vector') -> 'Vector':
        return Vector(self.x - other.x, self.y - other.y)

DistXY = Tuple[float, float, float]

class Curve:
    def __init__(self, points: List[Vector]) -> None:
        self.points = points

        loc = Vector(0, 0)
        nodes_x = [loc.x]
        nodes_y = [loc.y]
        for pt in points:
            loc += pt
            nodes_x.append(loc.x)
            nodes_y.append(loc.y)
        nodes = [nodes_x, nodes_y]
        self.curve = bezier.Curve(nodes, degree=len(self.points))
        self.dists: Optional[List[DistXY]] = None


    def evaluate(self) -> None:
        if self.dists is None:
            self.dists = []
            s = 0.0
            dist = 0.0
            last_x = last_y = 0.0
            while s < 1:
                [x], [y] = self.curve.evaluate(s)
                dx = last_x - x
                dy = last_y - y
                dist += math.hypot(dy, dx)
                self.dists.append((dist, x, y))
                last_x = x
                last_y = y
                s += 0.001

    def endpoint(self) -> Vector:
        loc = Vector(0, 0)
        for pt in self.points:
            loc += pt
        return loc

    def draw(self, ctx: cairo.Context, display: 'Output',
             start_x: float, start_y: float) -> None:

        x = start_x
        y = start_y        

        self.evaluate()

        midi_offset = bars_to_secs(0.25)

        if display.midi_time <= midi_offset:
            return

        end_fade = display.credits_start + 1
        if display.output_time < display.credits_start:
            alpha = 1.0
        elif display.output_time < end_fade:
            alpha = lscale(display.output_time, display.credits_start, end_fade, 1.0, 0.0)
        else:
            alpha = 0.0
            
        set_color(ctx, branch_color, alpha)

        ctx.set_line_width(display.height * 0.006)
        ctx.set_line_cap(cairo.LINE_CAP_ROUND)

        first_time = True
        assert self.dists is not None

        for dist_so_far, x, y in self.dists:
            if first_time:
                ctx.move_to(start_x + x, start_y + y)
                first_time = False
            else:
                ctx.line_to(start_x + x, start_y + y)
                if dist_so_far >= (display.midi_time - midi_offset) * display.grow_speed:
                    break

        ctx.stroke()

    def displaced(self, displacement: Vector) -> 'Curve':
        p1 = self.points[-1] + displacement
        p0 = self.points[-2]
        d = p0 + p1
        alpha = d.direction.r

        num = p0.magnitude ** 2 + d.magnitude ** 2 - p1.magnitude ** 2
        den = 2 * p0.magnitude * d.magnitude
        theta = math.acos(num / den)
        phi = math.asin(math.sin(theta) * d.magnitude / p1.magnitude)

        if p1.direction.r > alpha:
            new_p0_r = rad(alpha - theta)
            new_p1_r = rad(alpha + phi - theta)
        else:
            new_p0_r = rad(alpha + theta)
            new_p1_r = rad(alpha - phi + theta)


        # print('p0', p0)
        # print('p1', p1)
        # print('d', d)
        # print('alpha', rtod(alpha))
        # print('theta', rtod(theta))
        # print('phi', rtod(phi))
        # print(f'np0 {new_p0_r.d:8.1f}')
        # print(f'np1 {new_p1_r.d:8.1f}')


        # print(f'{rtod(alpha):8.1f} {rtod(theta):8.1f} {rtod(phi):8.1f}'
        #       f' | {new_p0_r.d:8.1f} {new_p1_r.d:8.1f}'
        #       f' | {self.points[-2].direction.d:8.1f}'
        #       f' {self.points[-1].direction.d:8.1f}')


        new_points = self.points[:-2]
        new_points.append(Vector.polar(new_p0_r, p0.magnitude))
        new_points.append(Vector.polar(new_p1_r, p1.magnitude))
        return Curve(new_points)


class Branch:
    def __init__(self, display: 'Output', angles: List[Union[float, Ang]], base_len: float) -> None:
        self.display = display
        self.angles = angles
        self.speed = self.display.width / 4.25
        self.marks: List['Marker'] = []

        assert len(angles) == 4
        lengths = [3 * base_len, 
                   2.6 * base_len, 
                   2 * base_len,
                   0.75 * base_len]

        cvecs = [Vector.polar(angle, length) 
                 for angle, length in zip(angles, lengths)]

        self.curve = Curve(cvecs)
        self.length = self.curve.curve.length
        self.duration = self.curve.curve.length / self.speed

    # make a marker that arrives at secs
    def mark(self, track: 'Track', strike_secs: float, release_secs: float, 
             color: int, decay: float) -> None:
        if get_barnum(strike_secs) < start_notes:
            return

        enter_secs = strike_secs - self.duration
        self.marks.append(Marker(track,
                                 enter_secs,
                                 strike_secs,
                                 release_secs,
                                 color,
                                 decay,
                                 self.speed))
        
    def draw(self, branches_ctx: cairo.Context, display: 'Output', origin: Vector, t: float) -> None:
        # undulate
        mag_scale = display.width / 100
        disp = Vector(rad(t), math.sin(0.5 * t) * mag_scale)

        newc = self.curve.displaced(disp)
        newc.draw(branches_ctx, display, origin.x, origin.y)
        
        notes_ctx = display.get_ctx('notes')
        for m in self.marks:
            if t < m.enter_t:
                pass
            elif t < m.strike_t:
                m.draw_on_stem(branches_ctx, display, origin, newc, t)
            else:
                m.draw_note(notes_ctx, origin, newc, t)

        while len(self.marks) > 0:
            m = self.marks[0]
            if t < m.release_t + m.decay:
                break
            self.marks.pop(0)


class Drum:
    def draw(self, ctx: cairo.Context, width: int, height: int, t: float) -> None:
        pass

class Kick(Drum):
    def __init__(self, track: 'Track') -> None:
        self.track = track
        self.alpha = 0
        self.strikes = []
        self.strike_count = 0
        self.start = -100.0

        for _, strike_t, _ in track.notes:
            self.strikes.append(strike_t)

        disp = track.display

        self.positions = [
            [disp.width * 0.17, disp.height * 0.85],
            [disp.width * 0.60, disp.height * 0.79],
            [disp.width * 0.77, disp.height * 0.85]
        ]
        
    def draw(self, ctx: cairo.Context, width: int, height: int, t: float) -> None:
        if get_barnum(t) < start_kick:
            return

        while len(self.strikes) > 0:
            if t < self.strikes[0]:
                break

            self.strike_count += 1
            self.start = t

            self.strikes.pop(0)
            
        age = t - self.start
        decay = 2.5
        alpha = lscale(age, 0, decay, 1, 0)
        for x, y in self.positions:
            self.draw_star(ctx, x, y, alpha)

    def draw_star(self, ctx: cairo.Context, x: float, y: float, alpha: float) -> None:
        center = Vector(x, y)

        ctx.set_source_rgba(1, .5, 0, alpha)

        width = self.track.display.width

        size = width / 320
        ctx.arc(center.x, center.y, size, 0, 2 * math.pi)
        ctx.fill()

        ctx.set_line_width(self.track.display.height * 0.005)

        r1 = width / 42
        r2 = width / 64
        r3 = width / 91
        ang1 = deg(30)

        ctx.set_line_cap(cairo.LINE_CAP_ROUND)

        bottom = center + Vector.polar(deg(-90), r1)
        top = center + Vector.polar(deg(90), r1)
        ctx.move_to(bottom.x, bottom.y)
        ctx.line_to(top.x, top.y)
        ctx.stroke()

        a = center + Vector.polar(ang1, r2)
        b = center + Vector.polar(ang1 + deg(180), r2)
        ctx.move_to(a.x, a.y)
        ctx.line_to(b.x, b.y)
        ctx.stroke()

        a = center + Vector.polar(-ang1, r2)
        b = center + Vector.polar(-ang1 + deg(180), r2)
        ctx.move_to(a.x, a.y)
        ctx.line_to(b.x, b.y)
        ctx.stroke()

        ctx.set_line_width(self.track.display.height * 0.003)
        a = center + Vector(-r3, 0)
        b = center + Vector(r3, 0)
        ctx.move_to(a.x, a.y)
        ctx.line_to(b.x, b.y)
        ctx.stroke()
        

class SmallStar:
    def __init__(self, disp: 'Output', x: float, y: float):
        self.disp = disp
        self.x = x
        self.y = y

    def draw(self, ctx: cairo.Context, age: float) -> None:
        decay = 3
        alpha = lscale_clamp(age, 0, decay, 1, 0)

        ctx.set_source_rgba(1, 1, 1, alpha)

        size = self.disp.height / 300
        ctx.arc(self.x, self.y, size, 0, 2 * math.pi)
        ctx.fill()

class HighHat(Drum):
    def __init__(self, track: 'Track') -> None:
        self.track = track
        self.alphas = [0, 0]
        self.strikes = []
        self.strike_count = -1
        self.starts: List = [-100.0, -100.00]
        for _, strike_t, _ in track.notes:
            self.strikes.append(strike_t)
        
        random.seed(2)

        width = track.display.width
        height = track.display.height

        nstars = 400
        stars: List[SmallStar] = []
        while len(stars) < nstars:
            x = int(random.uniform(0, width))
            y = int(random.gauss(height * 0.9, height * 0.3))
            if height * 0. < y < height:
                stars.append(SmallStar(track.display, x, y))

        self.stars = [stars[0:nstars // 2], stars[nstars // 2:]]

    def draw(self, ctx: cairo.Context, width: int, height: int, t: float) -> None:
        if get_barnum(t) < start_hh:
            while len(self.strikes) > 0 and self.strikes[0] <= t:
                self.strikes.pop(0)

            return

        while len(self.strikes) > 0:
            if t < self.strikes[0]:
                break

            self.strike_count += 1

            if self.strike_count % 4 == 0:
                self.starts[0] = t
            elif self.strike_count % 4 == 2:
                self.starts[1] = t

            self.strikes.pop(0)

        for phase in range(2):
            age = t - self.starts[phase]
            for star in self.stars[phase]:
                star.draw(ctx, age)


class Marker:
    def __init__(self, track: 'Track', 
                 enter_t: float, strike_t: float, release_t: float, 
                 color: int, decay: float, speed: float) -> None:
        self.track = track
        self.enter_t = enter_t
        self.strike_t = strike_t
        self.release_t = release_t
        self.color = color
        self.decay = decay
        self.speed = speed

    def draw_on_stem(self, ctx: cairo.Context, disp: 'Output', 
                     origin: Vector, curve: Curve, t: float) -> None:
        delta_t = t - self.enter_t
        desired_dist = delta_t * self.speed
        
        assert curve.dists is not None
        lower = 0
        upper = len(curve.dists) - lower
        while lower <= upper:
            middle = (lower + upper) // 2
            if middle >= len(curve.dists):
                break
            dist, x, y = curve.dists[middle]
            if dist <= desired_dist:
                lower = middle + 1
            elif dist > desired_dist:
                upper = middle - 1
        
        if lower >= len(curve.dists):
            lower = len(curve.dists) - 1
        _, x, y = curve.dists[lower]
        set_color(ctx, branch_color)

        size = disp.width / 200
        ctx.arc(x + origin.x, y + origin.y, size, 0, 2 * math.pi)
        ctx.fill()
        
    def draw_note(self, ctx: cairo.Context, origin: Vector, curve: Curve, t: float) -> None:
        age = t - self.release_t
        alpha = lscale_clamp(age, 0, self.decay, 1, 0)

        r = ((self.color >> 16) & 0xff) / 255.0
        g = ((self.color >> 8) & 0xff) / 255.0
        b = (self.color & 0xff) / 255.0

        width = self.track.display.width
        ball_size = width / 64

        ctx.set_source_rgba(r, g, b, alpha)
        endp = curve.endpoint()
        ctx.arc(endp.x + origin.x, endp.y + origin.y, ball_size, 0, 2 * math.pi)
        ctx.fill()
        
        if self.track.spikes:
            h, s, v = colorsys.rgb_to_hsv(r, g, b)
        
            new_h = math.fmod(h + 1 / 6, 1)
            new_s = .9
            new_v = .9
            new_r, new_g, new_b = colorsys.hsv_to_rgb(new_h, new_s, new_v)
            ctx.set_source_rgba(new_r, new_g, new_b, alpha)
            cx = endp.x + origin.x
            cy = endp.y + origin.y

            base = self.track.display.height * 0.025
            height = base * 3

            ctx.save()
            ctx.translate(cx, cy)

            if endp.x < 0:
                ctx.rotate(t * 0.5)
            else:
                ctx.rotate(-1 * t * 0.5)

            spike_ang = deg(28)
            spike_start = deg(90) - spike_ang
            spike_end = deg(90) + spike_ang

            for i in range(5):
                ctx.rotate(deg(360 / 5).r)
                ctx.arc(0, 0, ball_size * 1.10, spike_start.r, spike_end.r)
                ctx.line_to(0, height)
                ctx.close_path()
                ctx.fill()

            ctx.restore()


def draw_title(ctx: cairo.Context, disp: 'Output', t: float) -> None:
    x = disp.width * 0.15
    y = disp.height * 0.085
    title_start = 1
    title_up = title_start + 2
    title_stay = title_up + 2
    title_down = title_stay + 2

    if t < title_start:
        alpha = 0.0
    elif t < title_up:
        alpha = lscale(t, title_start, title_up, 0, 1)
    elif t < title_stay:
        alpha = 1
    elif t < title_down:
        alpha = lscale(t, title_stay, title_down, 1, 0)
    else:
        alpha = 0

    ctx.set_source_rgba(0, 0, 0, alpha)
    ctx.move_to(x, y)
    ctx.show_text('mylofy_svs02')

class Snippet:
    def __init__(self, ctx: cairo.Context, msg: str):
        self.ctx = ctx
        self.msg = msg
        extents = ctx.text_extents(msg)
        self.width = extents.x_advance
        self.height = extents.height
    
    def draw(self, x: float, y: float, alpha: float) -> None:
        self.ctx.move_to(x, y)
        self.ctx.show_text(self.msg)

def centered_text(ctx: cairo.Context, disp: 'Output', y: float, msg: str) -> float:
    extents = ctx.text_extents(msg)
    x = disp.width / 2 - extents.x_advance / 2
    ctx.move_to(x, y)
    ctx.show_text(msg)
    return extents.height

def draw_credits(ctx: cairo.Context, disp: 'Output', t: float) -> None:
    credit_up = disp.credits_start + 2

    if t < disp.credits_start:
        alpha = 0.0
    elif t < credit_up:
        alpha = lscale(t, disp.credits_start, credit_up, 0, 1)
    else:
        alpha = 1

    left: List[Optional[Snippet]] = []
    left.append(Snippet(ctx, 'Music'))
    left.append(None)
    left.append(Snippet(ctx, 'Vocals'))
    left.append(Snippet(ctx, ''))
    left.append(None)
    left.append(Snippet(ctx, 'Video'))

    right: List[Optional[Snippet]] = []
    right.append(Snippet(ctx, 'mylofy'))
    right.append(None)
    right.append(Snippet(ctx, 'mylofy'))
    right.append(Snippet(ctx, 'snuffles'))
    right.append(None)
    right.append(Snippet(ctx, 'pace'))


    y = disp.height * 0.85

    ctx.set_source_rgba(1, 1, 1, alpha)
    line_height = centered_text(ctx, disp, y, 'mylofy_svs02')

    y -= line_height * 2

    line_height = centered_text(ctx, disp, y, 'The unfa Community')

    y -= line_height * 3

    left_width = max([s.width for s in left if s is not None])
    right_width = max([s.width for s in right if s is not None])

    line_height = max([s.height for s in left + right if s is not None])

    hspace = disp.width / 10

    total_width = left_width + hspace + right_width
    indent = disp.width / 2 - total_width / 2

    x1 = indent
    x2 = indent + left_width + hspace

    for left_elt, right_elt in zip(left, right):
        if left_elt:
            left_elt.draw(x1, y, alpha)
        if right_elt:
            right_elt.draw(x2, y, alpha)

        if left_elt is None:
            y -= line_height * 1.5
        else:
            y -= line_height * 1.1






def do_draw(display: 'Output', output_time: float) -> None:
    cx0 = display.width / 2
    cy0 = 0
    r0 = 10
    cx1 = cx0
    cy1 = cy0
    r1 = display.height
    
    sky = cairo.RadialGradient(cx0, cy0, r0, cx1, cy1, r1)
    sky.add_color_stop_rgb(0, 0x38 / 255, 0x40 / 255, 0xd9 / 255)
    sky.add_color_stop_rgb(1, 0x0f / 255, 0x14 / 255, 0x66 / 255)
    
    bg_ctx = display.get_ctx('bg')
    bg_ctx.set_source(sky)
    bg_ctx.rectangle(0, 0, display.width, display.height)
    bg_ctx.fill()


    drums_ctx = display.get_ctx('drums')

    for drum in display.drums:
        drum.draw(drums_ctx, display.width, display.height, display.midi_time)

    planet_radius = display.width * 3.125
    planet_visible = display.width * 0.109375
    planet_offset = -1 * planet_radius + planet_visible


    planet = cairo.RadialGradient(cx0, cy0, r0, cx1, cy1, r1)
    planet.add_color_stop_rgb(0, 0xff / 255, 0xae / 255, 0x00 / 255)
    f = .3
    planet.add_color_stop_rgb(1, 0xa6 / 255 * f, 0x71 / 255 * f, 0x00 / 255 * f)
    
    bg_ctx.set_source(planet)

    bg_ctx.arc(display.width * 0.5, planet_offset, planet_radius, 0, 2 * math.pi)
    bg_ctx.fill()

    branches_ctx = display.get_ctx('branches')

    origin = Vector(display.width * 0.5, display.height * 0.10)
    for b in display.branches:
        b.draw(branches_ctx, display, origin, display.midi_time)

    draw_title(bg_ctx, display, output_time)
    draw_credits(bg_ctx, display, output_time)

    if isinstance(display, Display):
        bg_ctx.set_source_rgba(0, 0, 0, 1)
        bg_ctx.move_to(display.width * 0.9, display.height * 0.05)
        bg_ctx.show_text(f'{display.midi_time:.0f}')

        barnum = math.floor(secs_to_bars(display.midi_time)) + 1
        bg_ctx.move_to(display.width * 0.05, display.height * 0.05)
        bg_ctx.show_text(f'{barnum}')



def init_ctx(ctx: cairo.Context, display: 'Output') -> None:
    ctx.scale(1, -1)
    ctx.translate(0, -1 * display.height)

    font_size = display.width / 32
    m = cairo.Matrix()
    m.scale(font_size, - font_size)
    ctx.set_font_matrix(m)



class Output:
    def __init__(self) -> None:
        self.width = 0
        self.height = 0
        self.bpm = 0.0
        self.branches: List[Branch] = []
        self.drums: List[Drum] = []
        self.pad_head = 0.0
        self.pad_tail = 0.0
        self.seek = 0.0
        self.total_duration = 0.0
        self.desired_duration = 0.0
        self.midi_time = 0.0
        self.frame_num = 0
        self.output_time = 0.0
        self.credits_offset = 0.0
        self.credits_start = 0.0
        self.grow_speed = 1.0

    def get_ctx(self, purpose: str) -> cairo.Context:
        return cast(cairo.Context, None)

    def add_branch(self, b: Branch) -> None:
        self.branches.append(b)

    def add_drum(self, drum: Drum) -> None:
        self.drums.append(drum)


class Display(Output):
    def __init__(self, width: int, height: int) -> None:
        self.width = width
        self.height = height
        self.create_time = time.time()

        self.win = Gtk.Window(title='curve')
        self.win.connect('destroy', Gtk.main_quit)
        self.win.set_default_size(self.width, self.height)
        self.win.connect('key-press-event', self.key_press)
        gi.require_foreign('cairo')

        self.drawing_area = Gtk.DrawingArea()
        self.win.add(self.drawing_area)

        self.drawing_area.connect('draw', self.cairo_draw)

        GLib.timeout_add(30, self.tick)  # millisecs
        self.win.show_all()

        self.branches = []
        self.drums = []
        self.gtk_ctx: Optional[cairo.Context] = None

    def get_ctx(self, purpose: str) -> cairo.Context:
        assert self.gtk_ctx
        return self.gtk_ctx

    def cairo_draw(self, da: Any, ctx: cairo.Context) -> None:
        self.output_time = time.time() - self.create_time
        self.frame_num = int(self.output_time / 24.0)
        self.midi_time = self.output_time - self.pad_head + self.seek

        display = self
        init_ctx(ctx, display)

        self.gtk_ctx = ctx
        do_draw(self, self.output_time)

        
    def key_press(self, widget: Any, event: Any) -> None:
        if event.keyval == ord('q') or event.keyval == ord('w'):
            sys.exit(0)

    def tick(self) -> bool:
        self.drawing_area.queue_draw()
        return True


class Image(Output):
    def __init__(self, width: int, height: int) -> None:
        self.width = width
        self.height = height
        self.frames_per_second = 24.0
        self.branches = []
        self.drums = []

        self.surfaces: Dict[str, Tuple[cairo.Surface, cairo.Context]] = {}

    def get_ctx(self, purpose: str) -> cairo.Context:
        purpose = 'f'

        if purpose in self.surfaces:
            _, ctx = self.surfaces[purpose]
            return ctx

        sur = cairo.ImageSurface(cairo.FORMAT_ARGB32, self.width, self.height)
        ctx = cairo.Context(sur)
        display = self
        init_ctx(ctx, display)

        self.surfaces[purpose] = (sur, ctx)
        return ctx

    def draw(self, frame_num: int, output_time: float) -> None:
        self.surfaces = {}

        do_draw(self, output_time)

        for purpose in self.surfaces:
            sur, _ = self.surfaces[purpose]
            fname = f'frames/{purpose}{frame_num:04d}.png'
            sur.write_to_png(fname)

        self.surfaces = {}


def run(d: Output) -> None:
    if isinstance(d, Display):
        try:
            Gtk.main()
        except KeyboardInterrupt:
            sys.exit(0)
    elif isinstance(d, Image):
        nframes = math.ceil(d.total_duration * 24.0)
        process_start = time.time()
        for frame_num in range(nframes):
            elasped = time.time() - process_start
            if frame_num > 0:
                seconds_per_frame = elasped / frame_num
                frames_togo = nframes - frame_num
                remaining_secs = frames_togo * seconds_per_frame
                togo_min = int(remaining_secs // 60)
                togo_secs = int(math.floor(remaining_secs - togo_min * 60))
                pct = frame_num / nframes * 100

                print(f'{frame_num:4d} {frames_togo:4d} {seconds_per_frame:8.3f}s/f'
                      f' {pct:8.3f}% {remaining_secs:6.1f}'
                      f' {togo_min:4d}:{togo_secs:02d}')

            output_time = frame_num / 24.0

            d.frame_num = frame_num
            d.output_time = output_time
            d.midi_time = output_time - d.pad_head + d.seek

            d.draw(frame_num, output_time)

Note_Strike_Time = Tuple[int, float, float]

class Track:
    def __init__(self, display: Output, filename: str, color: int, 
                 decay: float, spikes: bool) -> None:
        self.display = display
        self.filename = filename
        self.color = color
        self.decay = decay
        self.notes: List = []
        self.spikes = spikes

        note_strikes = {}

        def strike(note_num: int, t: float) -> None:
            note_strikes[note_num] = t

        def release(note_num: int, t: float) -> None:
            if note_num in note_strikes:
                self.notes.append((note_num, note_strikes[note_num], t))

        with mido.MidiFile(filename) as inf:
            t = 0
            for msg in inf:
                delta = msg.time * 120 / display.bpm
                t += delta

                if msg.type == 'note_on' and (msg.channel + 1) != 10:
                    note_num = msg.note
                    if msg.velocity > 0:
                        strike(note_num, t)
                    else:
                        release(note_num, t)
                        
                elif msg.type == 'note_off' and (msg.channel + 1) != 10:
                    note_num = msg.note
                    release(note_num, t)


