import cairo

import gi

gi.require_version('PangoCairo', '1.0')
from gi.repository import PangoCairo, Pango

width = 640
height = 480

sur = cairo.ImageSurface(cairo.FORMAT_ARGB32, width, height)
ctx = cairo.Context(sur)


if False:
    ctx.set_source_rgb(1, 1, 1)
    ctx.rectangle(0, 0, width, height)
    ctx.fill()

font_map = PangoCairo.font_map_get_default()

layout = PangoCairo.create_layout(ctx)
pctx = layout.get_context()


y = height * .6

font_name = "DejaVu Sans"

desc = Pango.FontDescription()
desc.set_family(font_name)
desc.set_weight(Pango.Weight.BOLD)
desc.set_size(48 * Pango.SCALE)
layout.set_font_description(desc)

layout.set_markup('MLF 02')
layout.set_alignment(Pango.Alignment.LEFT)
ink_box, log_box = layout.get_extents()

text_width = log_box.width / Pango.SCALE
text_height = log_box.height / Pango.SCALE

ctx.move_to(50, y)
ctx.set_source_rgb(0, 0, 0)
PangoCairo.show_layout(ctx, layout)

y += text_height * 1.1


sur.write_to_png("f.png")
