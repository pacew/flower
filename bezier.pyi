from typing import List, Tuple

class Curve:
    def __init__(self, nodes: List, degree: int) -> None:
        self.length = 0.0

    def evaluate(self, s: float) -> Tuple[List[float], List[float]]:
        return ([0], [0])
