SHIMS = $(HOME)/.pyenv/shims

all: credits.png


title.png: title.py
	env PATH=$(SHIMS):$(PATH) python title.py

credits.png: credits.py
	env PATH=$(SHIMS):$(PATH) python credits.py


main:
	env PATH=$(SHIMS):$(PATH) python flower.py display

lint:
	env PATH=$(SHIMS):$(PATH) flake8 flower.py curvelib.py bezier.pyi mido.pyi
	env PATH=$(SHIMS):$(PATH) mypy flower.py curvelib.py
