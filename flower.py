import argparse
import subprocess
import re

from curvelib import (Branch, Display, HighHat, Image, Kick, deg, run, Output, Track,
                      bars_to_secs)


inname = 'mylofy/mylofy_svs02_2022-04-06_2040.wav'

parser = argparse.ArgumentParser()
parser.add_argument('mode', choices=['display', 'image'])
parser.add_argument('-d', required=False)
parser.add_argument('-s', required=False)
parser.add_argument('--hires', required=False, action='store_true')
args = parser.parse_args()

width = 640
height = 360

# 3840x2160
# 2560x1440
# 1920x1080
# 1280x720
# 640x360
if args.hires:
    width = 1280
    height = 720

def get_display(mode: str) -> Output:
    if mode == 'display':
        return Display(width, height)
    elif mode == 'image':
        return Image(width, height)
    else:
        assert False, 'bad mode'

display = get_display(args.mode)
display.bpm = 112.0
display.pad_head = 5.0
display.pad_tail = 1.0
display.credits_offset = -10

if args.d is not None:
    display.desired_duration = float(args.d)
    display.pad_head = 0
    display.pad_tail = 0
    display.credits_offset = 0
else:
    display.desired_duration = 0.0


if args.s is not None:
    display.seek = float(args.s)
else:
    display.seek = 0.0


branches = [
    # center bunch
    Branch(display, [deg(60), deg(130), deg(190), deg(270)], display.width * 0.11),
    Branch(display, [deg(60), deg(125), deg(185), deg(280)], display.width * 0.095),
    Branch(display, [deg(60), deg(130), deg(200), deg(240)], display.width * 0.085),
    Branch(display, [deg(60), deg(135), deg(200), deg(270)], display.width * 0.07),

    # left bunch
    Branch(display, [deg(125), deg(130), deg(195), deg(240)], display.width * 0.065 * 1.05),
    Branch(display, [deg(125), deg(145), deg(220), deg(240)], display.width * 0.06 * 1.15),
    Branch(display, [deg(125), deg(145), deg(200), deg(260)], display.width * 0.045 * 1.2),
    Branch(display, [deg(125), deg(145), deg(215), deg(245)], display.width * 0.035 * 1.25),

    # right bunch
    Branch(display, [deg(70), deg(10), deg(-25), deg(-40)], display.width * 0.04),
    Branch(display, [deg(70), deg(20), deg(-20), deg(-50)], display.width * 0.06),
    Branch(display, [deg(70), deg(60), deg(-20), deg(-40)], display.width * 0.06),
    Branch(display, [deg(70), deg(60), deg(0), deg(-30)], display.width * 0.07),

    

]

max_branch_length = max([b.length for b in branches])
grow_secs = bars_to_secs(4)
display.grow_speed = max_branch_length / grow_secs


tracks = []
decay = 1

tracks.append(Track(display, 'mylofy/Consolidated-Pad-1-t1-1.mid', 0x43bccd, decay, False))
tracks.append(Track(display, 'mylofy/Consolidated-SynthBass-1-t1-1.mid', 
                    0x662e9b, decay, False))
tracks.append(Track(display, 'mylofy/Consolidated-epiano-1.mid', 0xea3546, decay, False))
tracks.append(Track(display, 'mylofy/Consolidated-Rhodes-1-t1-1.mid', 0xea3546, decay, False))

# spikes
# 40 secs
tracks.append(Track(display, 'mylofy/Consolidated-Vitalium-1.mid', 0x43bccd, decay, True))

# 130 secs
tracks.append(Track(display, 'mylofy/Consolidated-Ld-1-t1-1.mid', 
                    0x662e9b, decay, True))

kick = Kick(Track(display, 'mylofy/Consolidated-Kick-1-t1-1.mid', 0, 0, False))
hh = HighHat(Track(display, 'mylofy/Consolidated-Hh-6-t1-1.mid', 0, 0, False))

# mylofy/Consolidated-Snare-1-t1-1.mid

res = subprocess.run('ffmpeg -i core.mp3', shell=True, capture_output=True, text=True)
parts = re.search('Duration: ([^,]*)', res.stderr)
assert parts, "can't parse ffmpeg output"
raw_duration = parts.group(1)
num_parts = [float(field) for field in raw_duration.split(':')]
mp3_duration = num_parts[0] * 3600 + num_parts[1] * 60 + num_parts[2]
print(f'mp3_duration {mp3_duration:.1f}')

if display.desired_duration == 0:
    display.desired_duration = mp3_duration

mp3_to_use = min(display.desired_duration, mp3_duration - display.seek) - 0.2
print(f'mp3_to_use {mp3_to_use:.1f}')


midi_duration = 0.0
for track in tracks:
    for note_num, strike_t, release_t in track.notes:
        branch = branches[note_num % 12]
        branch.mark(track, strike_t, release_t, track.color, track.decay)
        if release_t > midi_duration:
            midi_duration = release_t

display.total_duration = display.pad_head + mp3_to_use + display.pad_tail

print(f'total_duration {display.total_duration:.1f}')

display.credits_start = display.pad_head + mp3_to_use + display.credits_offset


cmd = []
cmd.append('#! /bin/sh')
cmd.append(f'ffmpeg -y -i {inname} -acodec mp3 core.mp3')
cmd.append(f'sox core.mp3 audio.mp3'
           f' trim {display.seek:.1f} {mp3_to_use:.1f}'
           f' pad {display.pad_head:.1f} {display.pad_tail:.1f}')

cmd.append('ffmpeg'
           ' -i audio.mp3'
           ' -r 24'
           ' -f image2'
           f' -s {width}x{height}'
           ' -i frames/f%04d.png'
           ' -vcodec libx264'
           ' -acodec copy'
           ' -crf 25'
           ' -pix_fmt yuv420p'
           ' -y'
           ' out.mp4')

with open('mkvid', 'w') as outf:
    outf.write('\n'.join(cmd))
    outf.write('\n')


for b in branches:
    display.add_branch(b)

display.add_drum(kick)
display.add_drum(hh)

run(display)
